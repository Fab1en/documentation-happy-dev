# Manifeste

Happy Dev est le rassemblement spontané d'humains bienveillants, réunis par ces valeurs communes :

## Nous aspirons à prendre notre pied dans le travail

Nous refusons de prendre part à toute activité qui nous rendrait malheureux ; l'épanouissement des membres du réseau est aussi importante que la recherche de revenus.

## Nous considérons nos clients comme nous considérons nos amis

Ils sont les porteurs de projets au sein du réseau, nous ne faisons pas les projets POUR eux, mais AVEC eux.

## Nous désirons avoir un impact positif sur le monde

Nous sommes conscients des enjeux de notre époque, et ne voulons pas rester simples spectateurs. Nous voyons dans les technologies numériques un puissant levier de transformation.

## Nous ne laissons pas une collaboration décevoir un client

Nous sommes solidaires et collectivement responsables du succès des projets. L'enthousiasme de nos clients est la clé de voûte qui assure la pérennité du réseau.

## La bonne humeur est un objectif assumé

Nous aspirons à ce qu'une joyeuse énergie se dégage du réseau, basée sur l'écoute et la compréhension mutuelle. Nous contribuons tous à l'ambiance générale par nos passions et nos extravagances.

## Notre métier est humain avant d'être technique

Nous pensons que l'excès de formalisme empêche l'épanouissement des relations humaines. C'est dans la confiance et l'écoute attentive des besoins que nous souhaitons honorer nos engagements.

## Nous aspirons à nous sentir libre

Le salariat est pour nous une forme d'asservissement. Happy Dev rend les membres du réseau autonomes et maîtres de leur destin.

## Le réseau est basé sur la contribution ouverte

Chacun est libre de se l'approprier et d'y apporter ce qui lui semble bon. Happy Dev n'est que le reflet de ces contributions.


*Tous les membres du réseau Happy Dev ont signé ce manifeste.*

